import Icon from 'react-native-vector-icons/AntDesign';
import React, { Component } from 'react'
import {
    createStackNavigator,
    createAppContainer,
    createBottomTabNavigator,
    createMaterialTopTabNavigator
} from 'react-navigation';

import HomeScreen from '../components/HomeScreen';
import RecentScreen from '../components/RecentScreen';
import ContactScreen from '../components/ContactScreen';
import SingleProfileScreen from '../components/DetailOfSingleContact'
const ContactAndSingleProfile = createStackNavigator(
    {
        AllContactList:{
            screen:ContactScreen,
            navigationOptions:{
                header:null
            }
        },
        SingleProfile:{
            screen: SingleProfileScreen,
        },
    },
    {
        initialRouteName: 'AllContactList',
        
    }
)

const PhoneNavigator = createMaterialTopTabNavigator(
    {
        Recents: RecentScreen,
        Contacts: ContactAndSingleProfile,
    },
    {
        navigationOptions:{
            header:null
        }
    }
)

const HomeScreenNavigator = createBottomTabNavigator(
    {
        Home: { screen: HomeScreen,
        navigationOptions:{
            header:null
        } },
        Phone: { screen: PhoneNavigator,
            navigationOptions:{
                header:null
            }  }
    },
    {
    
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                if (routeName === 'Home') {
                    return (
                        <Icon name="home" size={20} color="#fb3742" />
                    );
                } else {
                    return (
                        <Icon name="phone" size={20} color="#fb3742" />
                    );
                }
            },
            header:null,
        }),
        tabBarOptions: {
            activeTintColor: '#FF6F00',
            inactiveTintColor: '#263238',
            pressColor: 'blue',
            pressOpacity: 0.2
        },
    }
);

const HomeAndPhoneNavTab = createStackNavigator(
    {
        HomeScreen: {screen: HomeScreenNavigator},
    },
    {
        defaultNavigationOptions:{
            header:null
        }
    }
    
);

const AllScreens = createAppContainer(HomeAndPhoneNavTab);

class MainScreen extends Component {
    render() {
        return <AllScreens />
    }
}

export default MainScreen;