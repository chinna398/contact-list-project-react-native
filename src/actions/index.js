export const selectContact = () =>{
    
    return async function(dispatch, getState){
        await fetch('https://randomuser.me/api/?results=5000')
        .then((response) => response.json())
        .then((resData) => {
            dispatch({ type: 'GET_CONTACT', payload:  resData.results })
            // console.log('============data at action==========\n',resData.results)
        })
        .catch((err) => console.log(err))
        dispatch({ type: 'LOADING_SUCCESS', payload: false });
    }
}