import {combineReducers} from 'redux'

import ContactListReducer  from './ContactListReducer';
import LoadingStatusReducer from './LoadingStatusReducer'

export default combineReducers({

    ContactList: ContactListReducer,
    LoadingStatus: LoadingStatusReducer
})