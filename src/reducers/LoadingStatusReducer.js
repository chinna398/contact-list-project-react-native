export default ( state = true , action) => {
    switch(action.type){
        case 'LOADING_SUCCESS':
            return action.payload;
        default:
            return state;
    }
}