import React, {Component} from 'react';
import {View, Text, TextInput, Alert} from 'react-native';
// import moment from 'moment';
import Icon from 'react-native-vector-icons/AntDesign';

import Time from './common/Time'

class HomeScreen extends Component{

    render(){
        const { DateTimeContainerStyle, dateStyle, iconsContainerStyle,searchBarContainerStyle, searchStyle } = styles;
        const  date  = new Date().toLocaleDateString();
        // time = moment().utcOffset('+05:30').format(' hh:mm:ss a')
        return(
            <View style = {{ flex: 1 }}>
                <View style = {DateTimeContainerStyle }>
                    <Time />
                    <Text style= { dateStyle }>{ date }</Text>
                </View>
                <View style = { searchBarContainerStyle }>
                    <Icon style={{ paddingBottom:12}} name = "search1" size={20} />
                    <TextInput style = {searchStyle} placeholder ="Google Search"></TextInput>
                </View>
                <View style= {iconsContainerStyle }>
                        <Icon style={{ flex:20 }} name="left" size={30} color="#900" onPress= {() =>{Alert.alert('pressed on left button')}} />
                        <Icon style={{ flex:20 }} name="up" size={30} color="#900" onPress= {() =>{Alert.alert('pressed on up button')}} />
                        <Icon style={{ flex:20 }} name="down" size={30} color="#900" onPress= {() =>{Alert.alert('pressed on down button')}} />
                        <Icon style={{ flex:0 }} name="right" size={30} color="#900" onPress= {() =>{Alert.alert('pressed on right button')}} />
                </View>
            </View>
        );
    }

}

const styles = {
    DateTimeContainerStyle:{
        flex: 1,
        paddingTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        // backgroundColor: 'blue',
    },
    dateStyle:{
        color: 'blue',
        fontSize: 28
    },
    searchBarContainerStyle:{
        flex: 5,
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row',
        // backgroundColor: 'red',
    },
    searchStyle:{
        width: 100
    },
    iconsContainerStyle:{
        flex:1,
        paddingLeft: 30,
        paddingRight: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // backgroundColor: 'yellow',
    }

}

export default HomeScreen ;