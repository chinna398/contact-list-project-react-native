import React, {Component} from 'react';
import { View } from 'react-native';
import { createMaterialTopTabNavigator,createAppContainer} from 'react-navigation';

import RecentScreen from './RecentScreen';
import ContactScreen from './ContactScreen';

const PhoneNavigator = createMaterialTopTabNavigator(
    {
        Recents: RecentScreen,
        Contacts: ContactScreen
    }
)

const MainNavigation = createAppContainer(PhoneNavigator);

class PhoneScreen extends Component{

    render(){
        
        return <MainNavigation />
    }

}

export default PhoneScreen;