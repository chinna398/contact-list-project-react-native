//import a library
import React from 'react';
import {View} from 'react-native'

//create component
const CardSection = (props) =>{
    return(
        <View style={styles.containerStyles}>
            {props.children}
        </View>
    );
};

//make styles
const styles ={
    containerStyles:{
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative'

    }
}

//exsport the component
export default CardSection