import React, { Component } from 'react'
import { Text } from 'react-native';
import moment from 'moment';

class TimeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          time: moment().utcOffset('+05:30').format(' hh:mm:ss a')
        };
      }
      componentDidMount() {
        this.intervalID = setInterval(
          () => this.clock(),
          1000
        );
      }
      componentWillUnmount() {
        clearInterval(this.intervalID);
      }
      clock() {
        this.setState({
          time: moment().utcOffset('+05:30').format(' hh:mm:ss a')
        });
      }
      render() {
        return <Text style = { styles.timeStyle }>{this.state.time}</Text>
      }
}

const styles = {
    timeStyle: {
        color: 'blue',
        fontSize: 38
    }

}

export default TimeComponent;