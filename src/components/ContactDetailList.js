import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Alert } from 'react-native';

import Card from './common/Card';
import CardSection from './common/CardSection';

class ContactDetailList extends Component {

    render() {
        // console.log("===========data at ContactDetailList===========",this.props.contactData);
        const { thumbnailContainerStyle, thumbnailStyle, headerContentStyle, headerTextStyle } = styles;
        const { picture, name, cell, phone } = this.props.contactData;

        return (
            <Card>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleProfile',this.props.contactData)}>
                    <CardSection>
                        <View style={thumbnailContainerStyle}>
                            <Image
                                style={thumbnailStyle}
                                source={{ uri: picture.thumbnail }}
                            />
                        </View>
                        <View style={headerContentStyle}>
                            <Text style={headerTextStyle}>{name.title + "." + name.first + " " + name.last}</Text>
                            <Text>{phone + " , " + cell}</Text>
                        </View>
                    </CardSection>
                </TouchableOpacity>
            </Card>
        );

    }
}

const styles = {
    thumbnailContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    thumbnailStyle: {
        width: 50,
        height: 50
    },
    headerContentStyle: {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    headerTextStyle: {
        fontSize: 18
    }

}

export default ContactDetailList;