import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    Alert,
    StyleSheet,
    AsyncStorage,
    Keyboard
} from 'react-native';

class ProfileDetail extends Component {

    state = {
        name: { title: "", first: "", last: "" },
        phone: "",
        cell: "",
        picture: { "thumbnail": null, "large": null },
        editDetailStatus: true
    }

    componentDidMount() {
        const { name, cell, phone, picture } = this.props.navigation.state.params
        this.setState({
            name: {
                title: name.title,
                first: name.first,
                last: name.last
            },
            phone: phone,
            cell: cell,
            picture: {
                thumbnail: picture.thumbnail,
                large: picture.large
            },
            editDetailStatus: true
        })
        // AsyncStorage.removeItem('contactData')
    }

    saveDataInLocalStorage = () => {

        const { name, phone, cell, picture } = this.state
        this.setState({
            editDetailStatus: true
        })
        Keyboard.dismiss();
        // console.log('===========data at asyncstorage save==========',this.state)
        AsyncStorage.getItem('contactData')
            .then((preViousData) => {
                // console.log('previou local storage data===', JSON.parse(preViousData))
                preViousData = (preViousData == null) ? [] : JSON.parse(preViousData);
                preViousData.push({
                    "name": {
                        "title": name.title, "first": name.first, "last": name.last
                    },
                    "phone": phone,
                    "picture": {
                        "thumbnail": picture.thumbnail,
                        "large": picture.large
                    },
                    "cell": cell
                })
                // console.log('updated data===', preViousData)
                AsyncStorage.setItem('contactData', JSON.stringify(preViousData))
            })
        Alert.alert("Successfuly changed")
    }
    onChangeVlue(onChangeVlue, nameInState) {
        const { title, first, last } = this.state.name;
        this.setState({
            editDetailStatus: false
        })
        switch (nameInState) {
            case 'title': {
                this.setState({
                    name: { 
                        title: onChangeVlue,
                        first: first,
                        last: last
                    },
                })
                break
            }
            case 'first': {
                this.setState({
                    name: { 
                        first: onChangeVlue,
                        title: title,
                        last: last
                     },
                })
                break
            }
            case 'last': {
                this.setState({
                    name: { 
                        last: onChangeVlue,
                        first: first,
                        title: title
                    },
                })
                break
            }
            case 'phone': {
                this.setState({
                    phone: onChangeVlue,
                })
                break
            }
            case 'cell': {
                this.setState({
                    cell: onChangeVlue,
                })
                break
            }
        }
    }

    render() {
        // console.log('==============Naame=========:', this.state.name)
        // console.log("======single page props=========", this.props.navigation.state.params);
        const { cell, name, phone, picture } = this.state;
        const { imageContainerStyle, inputContainerStyle, buttonContainerStyle, textInputStyle, labelStyle } = styles;

        return (
            <View style={{ flex: 1 }}>
                <View style={imageContainerStyle}>
                    <Image
                        resizeMode={'cover'}
                        style={{ width: '100%', height: '100%' }}
                        source={{ uri: picture.large }}
                    />
                </View>
                <View style={inputContainerStyle}>
                    <View style={{ paddingRight: 5, flexDirection: 'row' }}>
                        <Text style={labelStyle} >Title Name:</Text>
                        <TextInput
                            style={textInputStyle}
                            onChangeText={title => this.onChangeVlue(title, 'title')}
                            value={name.title}
                        />
                    </View>
                    <View style={{ paddingRight: 5, flexDirection: 'row' }}>
                        <Text style={labelStyle} >First Name:</Text>
                        <TextInput
                            style={textInputStyle}
                            onChangeText={first => this.onChangeVlue(first, 'first')}
                            value={name.first}
                        />
                    </View>
                    <View style={{ paddingRight: 5, flexDirection: 'row' }}>
                        <Text style={labelStyle} >Last Name :</Text>
                        <TextInput
                            style={textInputStyle}
                            onChangeText={last => this.onChangeVlue(last, 'last')}
                            value={name.last}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={labelStyle}>Cell     :</Text>
                        <TextInput
                            style={textInputStyle}
                            onChangeText={cell => this.onChangeVlue(cell, 'cell')}
                            value={cell}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={labelStyle}>Phone:</Text>
                        <TextInput
                            style={textInputStyle}
                            onChangeText={phone => this.onChangeVlue(phone, 'phone')}
                            value={phone} />
                    </View>
                </View>
                <View style={buttonContainerStyle}>
                    <TouchableOpacity disabled = { this.state.editDetailStatus } onPress={this.saveDataInLocalStorage}>
                        <Text style={{ color: 'blue' }}>Save Changes</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    imageContainerStyle: {
        flex: 3,
        paddingTop: 10
    },
    inputContainerStyle: {
        flex: 3,
        flexDirection: 'column'
    },
    buttonContainerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInputStyle: {
        flex: 1,
        fontSize: 18,
        paddingTop: 0,
        paddingLeft: 5,
        paddingRight: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    labelStyle: {
        fontSize: 20,
        paddingLeft: 5,
        paddingRight: 5
    }
})

export default ProfileDetail;