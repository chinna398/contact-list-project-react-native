import React, { Component } from 'react';
import { View, AsyncStorage, FlatList, StyleSheet, Text } from 'react-native';

import ContactDetailList from './ContactDetailList'

class RecentScreen extends Component {
  constructor(props){
    super(props);
    this.state = { AllRecentData: [], isLoading: false };
  }

  renderComponent = (item) => {
    return (<ContactDetailList contactData={item} navigation={this.props.navigation} />)
  }

  render() {
    // console.log('data in recentScreeen========\n',this.state.AllRecentData);
    AsyncStorage.getItem('contactData').then((response) => {
      this.setState({
        AllRecentData: JSON.parse(response),
        isLoading: false
      });
    });
    const { userMessageContainer, textStyle } = styles;
    if(this.state.isLoading){
      return(
        <View style = { userMessageContainer }>
          <Text style = { textStyle }>loading................!</Text>
        </View>
      )
    }else if(this.state.AllRecentData == null || this.state.AllRecentData == ""){
      return(
        <View style = { userMessageContainer }>
          <Text style = { textStyle }>No data Found</Text>
        </View>
      )
    }
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={this.state.AllRecentData}
          renderItem={({ item }) => this.renderComponent(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  userMessageContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle:{
    fontSize: 18,
    color: 'blue'
  }
})

export default RecentScreen;