import React, { Component } from 'react';
import { FlatList, StyleSheet, View, TextInput } from 'react-native';
import { connect } from 'react-redux';

import { selectContact } from '../actions/index'
import ContactDetailList from './ContactDetailList'
import Loading from './common/Spinner';
import Icon from 'react-native-vector-icons/AntDesign';

class ContactScreen extends Component {

    state = { contactDataSource: [] }

    componentDidMount() {
        this.props.selectContact();
    }

    renderContactDetailList = (item) => {
        // console.log('inside the rendercontact function\n', item)
        return (<ContactDetailList contactData={item} navigation={this.props.navigation} />)
    }

    componentWillReceiveProps() {
        this.setState({
            contactDataSource: this.props.ContactList
        })
    }

    SearchFilterFunction = text => {
        const newData = this.props.ContactList.filter(function (item) {

            const itemData = item.name.first ? item.name.first.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });

        this.setState({
            contactDataSource: newData
        })
    }

    renderComponent() {
        if (this.props.LoadingStatus) {
            return <Loading size="large" />
        } else {
            return (
                <View>
                    <View style={styles.textInputStyle}>
                        <Icon style = {{paddingLeft: 10, paddingTop: 10 }} name = "search1" size = { 20 }/>
                        <TextInput
                            placeholder="Find With First Name..."
                            onChangeText={text => this.SearchFilterFunction(text)}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <FlatList
                        data={this.state.contactDataSource}
                        renderItem={({ item }) => this.renderContactDetailList(item)}
                        keyExtractor={item => item.phone}
                    />
                </View>
            )
        }
    }

    render() {
        return this.renderComponent()
    }
}

const styles = StyleSheet.create({
    textInputStyle: {
        height: 50,
        borderWidth: 1,
        paddingLeft: 10,
        borderColor: '#009688',
        backgroundColor: '#FFFFFF',
        flexDirection: 'row'
    }
})

const mapStateToProps = state => {
    return {
        ContactList: state.ContactList,
        LoadingStatus: state.LoadingStatus,
    }
}

export default connect(mapStateToProps, { selectContact })(ContactScreen);