import React,{ Component } from 'react';
import ScreenRoute from './routes/ScreenRoutes';

class App extends Component {

  render() {
    return <ScreenRoute />
  }
}

export default App;