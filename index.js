import {AppRegistry} from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import React from 'react';

import App from './src/App';
import {name as appName} from './app.json';
import reducer from './src/reducers';

const store = createStore(reducer,applyMiddleware(thunk));

const reduxApp = () =>(
    <Provider store = {store}>
        <App />
    </Provider>
)

AppRegistry.registerComponent(appName, () => reduxApp);
